нотатки що до покращення

Схоже що код не запускається з першого разу через "make up"

Застосунок приймає змінну оточення `DB_CONNECTION_STRING`

а в docker-compose.yml передавалось
```
  DB_HOST: backend
  DB_PORT: 27017
  DB_DATABASE: dev
```

TypeDI додано фактично для галочки, використовується як [ServiceLocator](https://en.wikipedia.org/wiki/Service_locator_pattern)


Деякі речі в проєкті overengineered, на приклад об'явлення роутів

До прикладу цей код

```ts
export class ReadingRoute implements Routes {
  public path = '/api/readings';
  public router = Router();
  public controller = new ReadingController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.controller.getAll);
    // ...
  }
}
```
Можна записати як

```ts

const controller = new ReadingController();
const router = Router();

this.router.get(`/api/readings`, this.controller.getAll);
// ...

export default router;
```

Структура діректорій концентрується на абстракціях (middlerwares, services, routes, controllers, ...)

Зручніше розбивати по домену, так виявити протікання залежностей
```
reading
  reading.controller
  reading.service
  reading.dto

comment
  comment.controller
  comment.service
  comment.dto
```

Також проєкт покладається на анемічну модель, але це більше суб'єктивний мінус