import { ReadingModel } from '@/models/reading.model';
import { HttpException } from '@exceptions/httpException';
import { Comment, CommentModel, IComment } from '@models/comment.model';
import { Types } from 'mongoose';
import { Service } from 'typedi';

@Service()
export class CommentService {
  public async like(id: string): Promise<Comment> {
    const updateOneById: Comment = await CommentModel.findByIdAndUpdate(
      { _id: new Types.ObjectId(id) },
      {
        $inc: {
          likesCounter: 1,
        },
      },
      { new: true },
    );
    if (!updateOneById) {
      throw new HttpException(409, `Comment with id "${id}" doesn't exist`);
    }

    return updateOneById;
  }

  public async findAll(): Promise<Comment[]> {
    const result: Comment[] = await CommentModel.find();
    return result;
  }

  public async findOneById(id: string): Promise<Comment> {
    const findOne: Comment = await CommentModel.findOne({ _id: new Types.ObjectId(id) });
    if (!findOne) {
      throw new HttpException(409, "Comment doesn't exist");
    }

    return findOne;
  }

  public async createOne(data: IComment): Promise<Comment> {
    const reading = await ReadingModel.findById(data.reading);
    if (!reading) {
      throw new HttpException(409, `Reading ${data.reading} doesn't exist`);
    }

    reading.commentsCounter++;
    await reading.save();
    return CommentModel.create(data);
  }

  public async updateOne(id: string, data: IComment): Promise<Comment> {
    if (data.reading) {
      const reading = await ReadingModel.findById(data.reading);
      if (!reading) {
        throw new HttpException(409, `Reading ${data.reading} doesn't exist`);
      }
    }

    const updateOneById: Comment = await CommentModel.findByIdAndUpdate({ _id: new Types.ObjectId(id) }, data, { new: true });
    if (!updateOneById) {
      throw new HttpException(409, `Comment with id "${id}" doesn't exist`);
    }

    return updateOneById;
  }

  public async deleteOne(id: string): Promise<Comment> {
    const deleteOneById: Comment = await CommentModel.findByIdAndDelete({ _id: new Types.ObjectId(id) });
    if (!deleteOneById) {
      throw new HttpException(409, `Comment with id "${id}" doesn't exist`);
    }

    const reading = await ReadingModel.findById(deleteOneById.reading);
    if (reading) {
      reading.commentsCounter--;
      await reading.save();
    }

    return deleteOneById;
  }
}
