import { IPropCreatedAt, IPropUpdatedAt } from '@interfaces/common.interface';
import { HydratedDocument, model, Model, models, Schema, Types } from 'mongoose';
import { ReadingModel } from './reading.model';

export interface IComment extends IPropCreatedAt, IPropUpdatedAt {
  name: string;
  text: string;
  avatarUrl: string;
  reading?: Types.ObjectId;
  likesCounter: number;
}

export type ICommentModel = Model<IComment, {}>;

export type Comment = HydratedDocument<IComment>;

const CommentSchema: Schema = new Schema<IComment, ICommentModel>(
  {
    name: {
      required: true,
      unique: true,
      type: String,
    },
    text: {
      required: true,
      type: String,
    },
    avatarUrl: {
      type: String,
    },
    reading: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: ReadingModel,
    },
    likesCounter: {
      required: true,
      type: Number,
      default: 0,
      validate: {
        validator: Number.isInteger,
        message: prop => `propPath: ${prop.path}; propValue:${prop.value}; Is not an integer value!`,
      },
    },
  },
  {
    versionKey: false,
    timestamps: true,
  },
);

export const CommentModel: ICommentModel = models.Reading ?? model<IComment, ICommentModel>('Comment', CommentSchema);
