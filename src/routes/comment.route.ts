import { CommentController } from '@/controllers/comment.controller';
import { CreateCommentDto } from '@/dtos/comment.dto';
import { Routes } from '@interfaces/routes.interface';
import { ValidationMiddleware } from '@middlewares/validation.middleware';
import { Router } from 'express';

export class CommentRoute implements Routes {
  public path = '/api/comments';
  public router = Router();
  public controller = new CommentController();

  constructor() {
    this.initializeRoutes();
  }

  private initializeRoutes() {
    this.router.get(`${this.path}`, this.controller.getAll);
    this.router.get(`${this.path}/:id`, this.controller.getOneById);
    this.router.post(`${this.path}`, ValidationMiddleware(CreateCommentDto), this.controller.createOne);
    this.router.put(`${this.path}/:id`, ValidationMiddleware(CreateCommentDto, true, true), this.controller.updateOne);
    this.router.delete(`${this.path}/:id`, this.controller.deleteOne);

    this.router.put(`${this.path}/:id/likes`, this.controller.like);
  }
}
