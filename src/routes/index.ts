import { CommentRoute } from '@routes/comment.route';
import { ReadingRoute } from '@routes/reading.route';

const routes = [new ReadingRoute(), new CommentRoute()];

export default routes;
