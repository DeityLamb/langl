import { Comment, IComment } from '@/models/comment.model';
import { CommentService } from '@services/comment.service';
import { isValidMongoObjectId } from '@utils/dto';
import { NextFunction, Request, Response } from 'express';
import { Container } from 'typedi';

export class CommentController {
  public service = Container.get(CommentService);

  public like = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id: string = req.params.id;

      isValidMongoObjectId(id);

      const updatedDoc: Comment = await this.service.like(id);

      res.status(200).json({ data: updatedDoc, message: 'liked' });
    } catch (error) {
      next(error);
    }
  };

  public getAll = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const findAll = await this.service.findAll();

      res.status(200).json({ data: findAll, message: 'findAll' });
    } catch (error) {
      next(error);
    }
  };

  public getOneById = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id: string = req.params.id;

      isValidMongoObjectId(id);

      const findOne = await this.service.findOneById(id);

      res.status(200).json({ data: findOne, message: 'findOne' });
    } catch (error) {
      next(error);
    }
  };

  public createOne = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const body: IComment = req.body;
      const createdDoc: Comment = await this.service.createOne(body);

      res.status(201).json({ data: createdDoc, message: 'created' });
    } catch (error) {
      next(error);
    }
  };

  public updateOne = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id: string = req.params.id;

      isValidMongoObjectId(id);

      const body: IComment = req.body;
      const updatedDoc: Comment = await this.service.updateOne(id, body);

      res.status(200).json({ data: updatedDoc, message: 'updated' });
    } catch (error) {
      next(error);
    }
  };

  public deleteOne = async (req: Request, res: Response, next: NextFunction) => {
    try {
      const id: string = req.params.id;

      isValidMongoObjectId(id);

      const deletedDoc: Comment = await this.service.deleteOne(id);

      res.status(200).json({ data: deletedDoc, message: 'deleted' });
    } catch (error) {
      next(error);
    }
  };
}
