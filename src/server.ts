import { App } from '@/app';
import routes from '@/routes';
import { MongooseClient } from '@clients/MongooseClient';

(async () => {
  await MongooseClient.connect();

  const app = new App(routes);

  const server = app.listen();
  server.setTimeout(180000); // 3 min
})();
