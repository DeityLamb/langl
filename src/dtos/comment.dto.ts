import { IComment } from '@/models/comment.model';
import { toMongoObjectId } from '@/utils/dto';
import { Transform } from 'class-transformer';
import { IsNotEmpty, IsOptional, IsString, IsUrl } from 'class-validator';
import { Types } from 'mongoose';

export class CreateCommentDto implements Required<Omit<IComment, 'likesCounter' | 'createdAt' | 'updatedAt'>> {
  @IsNotEmpty()
  @Transform(toMongoObjectId)
  public reading: Types.ObjectId;

  @IsNotEmpty()
  @IsString()
  public name: string;

  @IsNotEmpty()
  @IsString()
  public text: string;

  @IsOptional()
  @IsUrl()
  public avatarUrl: string;
}
